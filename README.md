# Build base image using podman

```bash
$ podman build -t ros2-fiot-bootc:latest .
```
# Or

## Pull from prebuilt image
``` bash
$ podman pull quay.io/saypaulgit/ros2-iron-fedora-bootc:latest
```

## Creating image

Follow documentation: [https://quay.io/repository/saypaulgit/ros2-iron-fedora-bootc]()